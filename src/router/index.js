import { createRouter, createWebHashHistory } from 'vue-router'

// Halaman mainlayout
import MainLayout from '../components/MainLayout.vue'

// Halaman home
import HomesView from '../views/HomesView.vue'

//Halaman Kurikulum
import Kurikulum from '../views/KurikulumView.vue'

//Halaman SDM
import Sdm from '../views/SdmView.vue'

//Halaman UP2AI
import Tim from '../views/Upai/TimView.vue'
import About from '../views/Upai/AboutView.vue'
import Contact from '../views/Upai/ContactView.vue'

//Halaman MBKM
import Mbkm from '../views/Mbkm/MbkmView.vue'
import Ismafo from '../views/Mbkm/IsmafoView.vue'
import Tsa from '../views/Mbkm/TsaView.vue'
import Msib from '../views/Mbkm/MsibView.vue'
import Kompetisi from '../views/Mbkm/KompetisiView.vue'

//Halaman Lainnya
import Unduhan from '../views/UnduhanView.vue'

//Halaman Login
import Login from '../views/LoginView.vue'
import Register from '../views/RegisterView.vue'
import Profile from '../views/ProfileView.vue'

//Halaman Login
import Pedoman from '../views/PedomanView.vue'


//Halaman belum dipakai
import Mhs from '../views/hold/MahasiswaView.vue'
import Dosen from '../views/hold/DosenView.vue'
import Umum from '../views/hold/UmumView.vue'
import Prodi from '../views/hold/ProdiView.vue'
import HomeView from '../views/hold/HomeView.vue'



const routes = [
  {
    path: '/',
    component: MainLayout,
    children:[
      {
        path: 'homes',
        name: 'homes',
        component: HomesView
      }, 
      {
        path: 'tim',
        name: 'tim',
        component: Tim
      }, 
      {
        path: 'about',
        name: 'about',
        component: About
        // component: function () {
        //   return import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
        // }
      }, 
      {
        path: 'contact',
        name: 'contact',
        component: Contact
      },
      {
        path: 'mbkm',
        name: 'mbkmn',
        component: Mbkm
      }, 
      {
        path: 'kurikulum',
        name: 'kurikulum',
        component: Kurikulum
      }, 
      {
        path: 'sdm',
        name: 'sdm',
        component: Sdm
      }, 
      {
        path: 'ismafo',
        name: 'ismafo',
        component: Ismafo
      },
      {
        path: 'tsa',
        name: 'tsa',
        component: Tsa
      }, 
      {
        path: 'msib',
        name: 'msib',
        component: Msib
      }, 
      {
        path: 'kompetisi',
        name: 'kompetisi',
        component: Kompetisi
      }, 
      {
        path: 'unduhan',
        name: 'unduhan',
        component: Unduhan
      }, 
      {
        path: 'login',
        name: 'login',
        component: Login,
        // meta: {
        //   guest: true
        // }
      }, 
      {
        path: 'register',
        name: 'register',
        component: Register
      }, 
      {
        path: 'pedoman',
        name: 'pedoman',
        component: Pedoman
      },
      {
        path: '/profile',
        name: 'Profile', 
        component: Profile,
        meta: {
          auth: true
        }
      },
      
    ]
  }, 
  {
    path: '/on',
    name: 'on',
    component: HomeView
  }
  ,     
  {
    path: '/mhs',
    name: 'mhs',
    component: Mhs
  },   
  {
    path: '/dosen',
    name: 'dosen',
    component: Dosen
  },   
  {
    path: '/prodi',
    name: 'prodi',
    component: Prodi
  },   
  {
    path: '/umum',
    name: 'umum',
    component: Umum
  },   
 
  
]

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes
})

export default router
