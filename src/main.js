import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Content from './components/Content.vue'
// import axios from 'axios'

import { setHeaderToken } from './utils/auth.js'

// createApp(App).use(store).use(router).mount('#app')

//variable global
const app = createApp(App)

 // Pemanggilan variable global dan penambahan content header dan content
app.component('content',Content).use(store).use(router).mount('#app')


// axios.defaults.baseURL = 'http://localhost:3000/user/'

Vue.config.productionTip = false

const token = localStorage.getItem('token');

if (token) { 
  setHeaderToken(token) 
} 

store.dispatch('get_user', token)
.then(() => {
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
}).catch((error) => {
  console.error(error);
})


